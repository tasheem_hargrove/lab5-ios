//
//  ViewController.swift
//  CS422L
//
//  Created by Jonathan Sligh on 1/29/21.
//

import UIKit
import CoreData

class MainViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource  {

    @IBOutlet var collectionView: UICollectionView!
    var sets: [FlashcardSetEntity] = [FlashcardSetEntity]()
    var selectedSet: FlashcardSetEntity?
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "FlashcardSetEntity")
        
        do {
            sets = try context.fetch(fetchRequest) as! [FlashcardSetEntity]
        } catch {
            print(error)
        }
        collectionView.delegate = self
        collectionView.dataSource = self
        makeThingsLookPretty()
    }

    
    @IBAction func addNewSet(_ sender: Any) {
        let newSet = NSEntityDescription.insertNewObject(forEntityName: "FlashcardSetEntity", into: context)
            as! FlashcardSetEntity
        
        newSet.id = UUID()
        newSet.title = "Title \(sets.count + 1)"
        sets.append(newSet)
        
        do {
            try context.save()
        } catch {
            print(error)
        }
        
        collectionView.reloadData()
    }
    
    func deleteSet(_ set: FlashcardSetEntity) {
        let index = sets.firstIndex(of: set)
        sets.remove(at: index!)
        context.delete(set)
        
        do {
            try context.save()
        } catch {
            print("Error in MainViewController -> deleteSet()")
            print(error)
        }
        
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sets.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SetCell", for: indexPath) as! FlashcardSetCollectionCell
        //setup method just makes it look nice
        cell.setup()
        cell.textLabel.text = sets[indexPath.row].title
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "GoToDetail") {
            let destinationVC = segue.destination as! FlashCardSetDetailViewController
            destinationVC.mainVC = self
            destinationVC.currentSet = selectedSet
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //go to new view
        selectedSet = sets[indexPath.row]
        performSegue(withIdentifier: "GoToDetail", sender: self)
    }
    
    //another function to make things look nice
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let noOfCellsInRow = 2   //number of column you want
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))

        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))
        return CGSize(width: size, height: size)
    }
    
    //just a function to make things look nice
    func makeThingsLookPretty()
    {
        let margin: CGFloat = 10
        guard let collectionView = collectionView, let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else { return }
        flowLayout.minimumInteritemSpacing = margin
        flowLayout.minimumLineSpacing = margin
        flowLayout.sectionInset = UIEdgeInsets(top: margin, left: margin, bottom: margin, right: margin)
    }
    
    
}

