//
//  EditAlertViewController.swift
//  CS422L
//
//  Created by Jonathan Sligh on 2/18/21.
//

import UIKit
import CoreData

class EditAlertViewController: UIViewController {

    @IBOutlet var alertView: UIView!
    @IBOutlet var termEditText: UITextField!
    @IBOutlet var definitionEditText: UITextField!
    //card from FlashCardSetDetailViewController
    var card: FlashcardEntity?
    //use this later to do things to the flashcards potentially
    var parentVC: FlashCardSetDetailViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setup()
    }
    
    func setup()
    {
        alertView.layer.cornerRadius = 8.0
        //set term/def
        termEditText.text = card?.term
        definitionEditText.text = card?.definition
        //make it so it shows this is editable
        termEditText.becomeFirstResponder()
    }
    
    @IBAction func deleteFlashcard(_ sender: Any) {
        if(card != nil) {
            parentVC?.removeCard(card!)
        }
        
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func doneEditing(_ sender: Any) {
        self.dismiss(animated: false, completion: { [self] in
            self.card?.term = termEditText.text
            card?.definition = definitionEditText.text
            if self.card != nil {
                do {
                    let context: NSManagedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
                    try context.save()
                } catch {
                    print("Error in EditAlertViewController -> doneEditing()")
                    print(error)
                }
                parentVC?.tableView.reloadData()
            }
        })
    }
}
